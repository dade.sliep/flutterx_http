import 'dart:io';

import 'package:example/api.dart';
import 'package:example/dto.dart';
import 'package:flutter/material.dart';
import 'package:flutterx_http/flutterx_http.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) => MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutterx HTTP Demo',
      theme: ThemeData(primarySwatch: Colors.lightGreen),
      home: const HTTPExample());
}

class HTTPExample extends StatefulWidget {
  const HTTPExample({super.key});

  @override
  State<HTTPExample> createState() => _HTTPExampleState();
}

class _HTTPExampleState extends State<HTTPExample> {
  static const String _apiKey = 'f8af9d32473e8c46afeeb160d6306a28';
  final OpenWeather _api = OpenWeather(_apiKey);
  final TextEditingController _city = TextEditingController(text: 'Rome, IT');
  Units _units = Units.metric;
  Units _currentUnits = Units.metric;
  Result<WeatherInfo>? _data;
  bool _calling = false;

  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(title: const Text('HTTP example')),
      body: Center(
          child: Padding(
              padding: const EdgeInsets.all(32),
              child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
                Row(children: [
                  SizedBox(
                      width: MediaQuery.of(context).size.width * .6,
                      height: 38,
                      child: TextField(
                          controller: _city,
                          decoration:
                              const InputDecoration(isDense: true, icon: Icon(Icons.location_pin), hintText: 'City'))),
                  DropdownButton<Units>(
                      value: _units,
                      onChanged: (value) => setState(() => _units = value ?? _units),
                      items: Units.values
                          .map((unit) => DropdownMenuItem<Units>(value: unit, child: Text(unit.unit)))
                          .toList()),
                ]),
                const Spacer(),
                ..._weatherPreview,
                const Spacer(flex: 2),
              ]))),
      floatingActionButton: FloatingActionButton(
          onPressed: _getWeatherPreview, tooltip: 'Get weather', child: Icon(_calling ? Icons.block : Icons.wb_sunny)));

  Iterable<Widget> get _weatherPreview {
    if (_calling) return const [Text('Performing call...')];
    final result = _data;
    if (result == null)
      return const [Text('Welcome to flutterx_http API sample.\nPress the FAB to get weather preview for your city')];
    return result.handle<Iterable<Widget>>(
        onSuccess: (value) sync* {
          for (final weather in value.weather)
            yield Row(mainAxisSize: MainAxisSize.min, children: [
              Image.network(_api.imageUrl(weather), width: 42, height: 42),
              Text(weather.description.capitalized, style: Theme.of(context).textTheme.titleMedium),
            ]);
          yield Text(
              'There are ${value.main.temp.round()}${_currentUnits.unit} in ${value.name} (${value.sys.country})\nmin: ${value.main.tempMin.round()}${_currentUnits.unit}\nmax: ${value.main.tempMax.round()}${_currentUnits.unit}\nhumidity: ${value.main.humidity}%',
              style: Theme.of(context).textTheme.bodyMedium);
        },
        onError: (error) => [Text('Failed to get weather preview:\n${error.error}')]);
  }

  final CancellationToken _token = CancellationToken();

  Future<void> _getWeatherPreview() async {
    final client = XHttpClient();
    await Request.post(
        url: Uri.parse('http://asset-one.com/api/graphs/threads/41652e6a-ef58-4aef-845e-653cb7ba01c0/runs/stream'),
        body: {
          "input": {
            "messages": [
              {"content": "Ciao", "type": "human"}
            ]
          },
          "config": {
            "recursionLimit": 100,
            "configurable": {
              "model": "ollama",
              "system_prompt": "You are a helpful AI assistant. System time: {system_time}",
              "thread_is": "1efcf3a9-d47f-6189-8017-5a6c58d3a6db",
              "thread_id": "41652e6a-ef58-4aef-845e-653cb7ba01c0"
            }
          },
          "stream_mode": ["messages"],
          "assistant_id": "7c83a8ba-d4b6-5d50-9548-9891a893e17e",
          "checkpoint_id": "1efcf3a9-d47f-6189-8017-5a6c58d3a6db",
          "multitask_strategy": "rollback"
        },
        headers: const {
          HttpHeaders.acceptHeader: 'text/event-stream'
        }).send(client).asEventStream().then((stream) async {
      print('>     START');
      await for (final x in stream) {
        print('>     AAA ${x.event}');
      }
      print('>     FINISH');
    });
  }
}
