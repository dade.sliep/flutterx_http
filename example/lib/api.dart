import 'package:example/dto.dart';
import 'package:flutterx_http/flutterx_http.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

class OpenWeather {
  final XHttpClient _client;

  OpenWeather(String apiKey)
      : _client = XHttpClient(baseUrl: 'https://cors-anywhere.herokuapp.com/https://api.openweathermap.org/data/2.5')
          ..addInterceptor(RequestTransformInterceptor(
              (request) => request.copy(url: request.url.appendQueryParameters({'appid': apiKey}))))
          ..addInterceptor(const RequestHeadersFormatterInterceptor())
          ..addInterceptor(LoggableInterceptor())
          ..addInterceptor(const ResponseHeadersFormatterInterceptor());

  Future<WeatherInfo> getWeather({required String city, Units units = Units.standard, CancellationToken? cancel}) =>
      Request.get(url: Uri.parse('/weather'), params: {'q': city, 'units': units.name})
          .send(_client, options: XHttpRequestOptions(cancel: cancel))
          .asJsonObject(WeatherInfo.fromJson);

  String imageUrl(WWeather weather) => 'https://openweathermap.org/img/wn/${weather.icon}@2x.png';
}

enum Units { standard, metric, imperial }

extension UnitsExt on Units {
  String get unit {
    switch (this) {
      case Units.standard:
        return '°K';
      case Units.metric:
        return '°C';
      case Units.imperial:
        return '°F';
    }
  }
}
