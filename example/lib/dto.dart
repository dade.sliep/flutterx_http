import 'package:flutterx_utils/flutterx_utils.dart';

class WeatherInfo with DTO {
  final WCoord coord;
  final List<WWeather> weather;
  final String base;
  final WMain main;
  final int visibility;
  final WWind wind;
  final WClouds clouds;
  final int dt;
  final WSys sys;
  final int timezone;
  final int id;
  final String name;
  final int cod;

  const WeatherInfo({
    required this.coord,
    required this.weather,
    required this.base,
    required this.main,
    required this.visibility,
    required this.wind,
    required this.clouds,
    required this.dt,
    required this.sys,
    required this.timezone,
    required this.id,
    required this.name,
    required this.cod,
  });

  factory WeatherInfo.fromJson(JsonObject json) => WeatherInfo(
      coord: json.requireJsonObject('coord', WCoord.fromJson),
      weather: json.getJsonList('weather', WWeather.fromJson),
      base: json['base'],
      main: WMain.fromJson(json['main']),
      visibility: json['visibility'],
      wind: WWind.fromJson(json['wind']),
      clouds: WClouds.fromJson(json['clouds']),
      dt: json['dt'],
      sys: WSys.fromJson(json['sys']),
      timezone: json['timezone'],
      id: json['id'],
      name: json['name'],
      cod: json['cod']);

  @override
  JsonObject toJson() => {
        'coord': coord.toJson(),
        'weather': weather.toJsonList(),
        'base': base,
        'main': main.toJson(),
        'visibility': visibility,
        'wind': wind.toJson(),
        'clouds': clouds.toJson(),
        'dt': dt,
        'sys': sys.toJson(),
        'timezone': timezone,
        'id': id,
        'name': name,
        'cod': cod
      };
}

class WCoord with DTO {
  final double lon;
  final double lat;

  const WCoord({
    required this.lon,
    required this.lat,
  });

  factory WCoord.fromJson(JsonObject json) => WCoord(
        lon: json['lon'],
        lat: json['lat'],
      );

  @override
  JsonObject toJson() => {'lon': lon, 'lat': lat};
}

class WWeather with DTO {
  final int id;
  final String main;
  final String description;
  final String icon;

  WWeather({
    required this.id,
    required this.main,
    required this.description,
    required this.icon,
  });

  factory WWeather.fromJson(JsonObject json) => WWeather(
        id: json['id'],
        main: json['main'],
        description: json['description'],
        icon: json['icon'],
      );

  @override
  JsonObject toJson() => {
        'id': id,
        'main': main,
        'description': description,
        'icon': icon,
      };
}

class WMain with DTO {
  final double temp;
  final double feelsLike;
  final double tempMin;
  final double tempMax;
  final int pressure;
  final int humidity;
  final int seaLevel;
  final int grndLevel;

  WMain({
    required this.temp,
    required this.feelsLike,
    required this.tempMin,
    required this.tempMax,
    required this.pressure,
    required this.humidity,
    required this.seaLevel,
    required this.grndLevel,
  });

  factory WMain.fromJson(JsonObject json) => WMain(
        temp: json['temp'],
        feelsLike: json['feels_like'],
        tempMin: json['temp_min'],
        tempMax: json['temp_max'],
        pressure: json['pressure'],
        humidity: json['humidity'],
        seaLevel: json['sea_level'] ?? -1,
        grndLevel: json['grnd_level'] ?? -1,
      );

  @override
  JsonObject toJson() => {
        'temp': temp,
        'feels_like': feelsLike,
        'temp_min': tempMin,
        'temp_max': tempMax,
        'pressure': pressure,
        'humidity': humidity,
        'sea_level': seaLevel,
        'grnd_level': grndLevel,
      };
}

class WWind with DTO {
  final double speed;
  final int deg;
  final double gust;

  WWind({
    required this.speed,
    required this.deg,
    required this.gust,
  });

  factory WWind.fromJson(JsonObject json) => WWind(
        speed: json['speed'] ?? -1,
        deg: json['deg'] ?? -1,
        gust: json['gust'] ?? -1,
      );

  @override
  JsonObject toJson() => {
        'speed': speed,
        'deg': deg,
        'gust': gust,
      };
}

class WClouds with DTO {
  final int all;

  WClouds({
    required this.all,
  });

  factory WClouds.fromJson(JsonObject json) => WClouds(
        all: json['all'],
      );

  @override
  JsonObject toJson() => {
        'all': all,
      };
}

class WSys with DTO {
  final int type;
  final int id;
  final String country;
  final int sunrise;
  final int sunset;

  WSys({
    required this.type,
    required this.id,
    required this.country,
    required this.sunrise,
    required this.sunset,
  });

  factory WSys.fromJson(JsonObject json) => WSys(
        type: json['type'],
        id: json['id'],
        country: json['country'],
        sunrise: json['sunrise'],
        sunset: json['sunset'],
      );

  @override
  JsonObject toJson() => {
        'type': type,
        'id': id,
        'country': country,
        'sunrise': sunrise,
        'sunset': sunset,
      };
}
