# flutterx_http

A useful wrapper library of dart.dev http library for HTTP requests.

## Import

Import the library like this:

```dart
import 'package:flutterx_http/flutterx_http.dart';
```

## Usage

Check the documentation in the desired source file of this library