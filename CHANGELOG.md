## 1.5.2

* Add body to delete

## 1.5.1

* Improve SSE support

## 1.5.0

* Add SSE support for html

## 1.4.7

* Fix types

## 1.4.6

* Code improvements

## 1.4.5

* Fix io adapter

## 1.4.4

* Add No auth check in interceptor

## 1.4.3

* Add UnauthorizedInterceptor

## 1.4.2

* Fix converter

## 1.4.1

* Update dependencies

## 1.4.0

* Add debug functionalities

## 1.3.16

* Improve headers management

## 1.3.15

* Fix loggable interceptor, add headers formatter interceptor

## 1.3.14

* Fix bool

## 1.3.13

* Improve response converters

## 1.3.12

* Improve log interceptor

## 1.3.11

* Improve log interceptor

## 1.3.10

* Add mock interceptor

## 1.3.8

* Add response converter methods

## 1.3.7

* Add exception response in toString

## 1.3.6

* Improve multipart request

## 1.3.5

* Fixes

## 1.3.4

* Improvements

## 1.3.3

* Fix AuthorizationInterceptor

## 1.3.2

* Change XHttpError to XHttpException

## 1.3.1

* Improve code

## 1.3.0

* Remove http dependency, refactor client structure

## 1.2.0

* Improvements

## 1.1.9-dev

* Refactoring

## 1.1.7-dev

* Add client in callback

## 1.1.6-dev

* Add cancellation

## 1.1.5-dev

* Fix TransformRequest

## 1.1.4-dev

* Add EncodedData

## 1.1.3-dev

* Refactoring

## 1.1.2-dev

* Add transform request

## 1.1.0

* Initial stable release

## 1.0.3-dev

* Moved DTO extension to flutterx_utils.

## 1.0.2-dev

* Moved DTO to flutterx_utils.

## 1.0.1-dev

* Improvements, add utilities, improve example project.

## 1.0.0-dev

* Initial release.
