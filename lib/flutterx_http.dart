library flutterx_http;

import 'dart:collection';

export 'src/adapter/adapter.dart';
export 'src/interceptor/interceptor_authorization.dart';
export 'src/interceptor/interceptor_base_url.dart';
export 'src/interceptor/interceptor_headers_formatter.dart';
export 'src/interceptor/interceptor_loggable.dart';
export 'src/interceptor/interceptor_mock.dart';
export 'src/interceptor/interceptor_request_transform.dart';
export 'src/interceptor/interceptor_unauthorized.dart';
export 'src/request.dart';
export 'src/request_application.dart';
export 'src/request_multipart.dart';
export 'src/response_converter.dart';

typedef QueryParameters = Map<String, String>;

typedef HeadersMap = Map<String, String>;

extension UriExt on Uri {
  Uri appendQueryParameters(QueryParameters params) => params.isEmpty
      ? this
      : replace(queryParameters: {
          ...queryParametersAll,
          ...params.map(MapEntry.new),
        });
}

HeadersMap headersMap() => LinkedHashMap<String, String>(
    equals: (a, b) => a.toLowerCase() == b.toLowerCase(), hashCode: (key) => key.toLowerCase().hashCode);
