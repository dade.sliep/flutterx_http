import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutterx_http/flutterx_http.dart';

@immutable
class MultipartRequest extends IRequest {
  static final RegExp _asciiOnly = RegExp(r'^[\x00-\x7F]+$');
  final String method;
  final Uri url;
  final QueryParameters params;
  final HeadersMap headers;
  final Map<String, String> fields;
  final List<MultipartFile> files;
  final Encoding encoding;

  const MultipartRequest({
    this.method = 'POST',
    required this.url,
    this.params = const {},
    this.headers = const {},
    this.fields = const {},
    this.files = const [],
    this.encoding = utf8,
  });

  @override
  XHttpRequest finalize() {
    final boundary = 'dart-http-boundary-${Random().nextInt(4294967296).toString().padLeft(10, '0')}';
    final headers = headersMap()
      ..addAll(this.headers)
      ..[HttpHeaders.contentTypeHeader] = 'multipart/form-data; boundary=$boundary';
    return XHttpRequest(
        method: method,
        url: url.appendQueryParameters(params),
        headers: headers,
        body: XHttpContent.fromStream(_finalize(boundary)));
  }

  Stream<List<int>> _finalize(String boundary) async* {
    final separator = encoding.encode('--$boundary\r\n');
    final line = encoding.encode('\r\n');
    for (final field in fields.entries) {
      yield separator;
      yield encoding.encode(_headerForField(field.key, field.value));
      yield encoding.encode(field.value);
      yield line;
    }
    for (final file in files) {
      yield separator;
      yield encoding.encode(_headerForFile(file));
      yield* file.content.asStream();
      yield line;
    }
    yield encoding.encode('--$boundary--\r\n');
  }

  static String _headerForField(String name, String value) {
    var header = 'content-disposition: form-data; name="${MultipartFile.encodeName(name)}"';
    if (!_asciiOnly.hasMatch(value))
      header = '$header\r\n'
          'content-type: text/plain; charset=utf-8\r\n'
          'content-transfer-encoding: binary';
    return '$header\r\n\r\n';
  }

  static String _headerForFile(MultipartFile file) {
    var header = 'content-type: ${file.contentType}\r\n'
        'content-disposition: form-data; name="${MultipartFile.encodeName(file.field)}"';
    final filename = file.filename;
    if (filename != null) header = '$header; filename="${MultipartFile.encodeName(filename)}"';
    return '$header\r\n\r\n';
  }
}
