import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

import 'package:flutterx_http/flutterx_http.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

extension ResponseFutureConverter on Future<XHttpResponse> {
  Future<Uint8List> asBytes() => then((response) => response.asBytes());

  Future<Stream<List<int>>> asBytesStream() => then((response) => response.asBytesStream());

  Future<Stream<EventStream>> asEventStream() => then((response) => response.asEventStream());

  Future<bool> asBool() => then((response) => response.asBool());

  Future<int> asInt() => then((response) => response.asInt());

  Future<double> asDouble() => then((response) => response.asDouble());

  Future<String> asText() => then((response) => response.asText());

  Future<T> asJson<T>(FromJson<T> fromJson) => then((response) => response.asJson<T>(fromJson));

  Future<T> asJsonObject<T>(FromJsonObject<T> fromJson) => then((response) => response.asJsonObject<T>(fromJson));

  Future<List<E>> asList<E>() => then((response) => response.asList<E>());

  Future<List<E>> asJsonList<E>(FromJsonObject<E> fromJson) => then((response) => response.asJsonList<E>(fromJson));

  Future<Map<K, V>> asJsonMap<K, V>({FromJson<K>? fromJsonKey, FromJson<V>? fromJsonValue}) =>
      then((response) => response.asJsonMap<K, V>(fromJsonKey: fromJsonKey, fromJsonValue: fromJsonValue));
}

extension ResponseConverter on XHttpResponse {
  Future<Uint8List> asBytes() => body.asBytes();

  Stream<List<int>> asBytesStream() => body.asStream();

  Stream<EventStream> asEventStream() => body.asEventStream();

  Future<bool> asBool() => asText().then(parseBool);

  Future<int> asInt() => asText().then(int.parse);

  Future<double> asDouble() => asText().then(double.parse);

  Future<String> asText() => body.asBytes().then(encoding.decode);

  Future<T> asJson<T>(FromJson<T> fromJson) => asText().then((json) => fromJson(jsonDecode(json)));

  Future<T> asJsonObject<T>(FromJsonObject<T> fromJson) => asJson<T>((json) => fromJson(json));

  Future<List<E>> asList<E>() => asJson((json) => fromJsonList<E>(json));

  Future<List<E>> asJsonList<E>(FromJsonObject<E> fromJson) =>
      asJson((json) => fromJsonList<E>(json, fromJson: (json) => fromJson(json)));

  Future<Map<K, V>> asJsonMap<K, V>({FromJson<K>? fromJsonKey, FromJson<V>? fromJsonValue}) =>
      asJson((json) => fromJsonMap<K, V>(json, fromJsonKey: fromJsonKey, fromJsonValue: fromJsonValue));
}
