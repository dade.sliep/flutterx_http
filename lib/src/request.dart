import 'package:flutter/foundation.dart';
import 'package:flutterx_http/src/adapter/adapter.dart';

abstract class IRequest {
  const IRequest();

  @protected
  XHttpRequest finalize();

  Future<XHttpResponse> send(XHttpClient client, {XHttpRequestOptions options = const XHttpRequestOptions()}) =>
      client.send(finalize(), options: options);
}
