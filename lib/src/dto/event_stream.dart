part of '../adapter/adapter.dart';

@immutable
class EventStream {
  final String data;
  final String? event;
  final String? id;
  final int? retry;

  const EventStream({required this.data, required this.event, required this.id, required this.retry});

  @override
  bool operator ==(Object other) =>
      other is EventStream && data == other.data && event == other.event && id == other.id && retry == other.retry;

  @override
  int get hashCode => Object.hash(data, event, id, retry);

  static void parseEventStream(Iterable<String> input, StreamSink<EventStream> sink) {
    final buffer = StringBuffer();
    String? event;
    String? id;
    int? retry;
    for (var line in input) {
      line = line.trim();
      if (line.isEmpty) {
        if (buffer.isNotEmpty) {
          sink.add(EventStream(data: buffer.toString(), event: event, id: id, retry: retry));
          buffer.clear();
          event = null;
          id = null;
          retry = null;
        }
      } else {
        if (line.startsWith('data:')) {
          if (buffer.isNotEmpty) buffer.write('\n');
          buffer.write(line.substring(5).trim());
        } else if (line.startsWith('event:')) {
          event = line.substring(6).trim();
        } else if (line.startsWith('id:')) {
          id = line.substring(3).trim();
        } else if (line.startsWith('retry:')) {
          retry = int.tryParse(line.substring(6).trim());
        }
      }
    }
    if (buffer.isNotEmpty) sink.add(EventStream(data: buffer.toString(), event: event, id: id, retry: retry));
  }
}
