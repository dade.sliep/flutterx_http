part of '../adapter/adapter.dart';

@immutable
class MultipartFile {
  static final RegExp _newlineRegExp = RegExp(r'\r\n|\r|\n');
  final String field;
  final String? filename;
  final MediaType contentType;
  final XHttpContent content;

  MultipartFile.fromBytes({
    required String field,
    String? filename,
    required this.contentType,
    required Uint8List bytes,
  })  : field = encodeName(field),
        filename = filename == null ? null : encodeName(filename),
        content = XHttpContent.fromBytes(bytes);

  MultipartFile.fromStream({
    required String field,
    String? filename,
    required this.contentType,
    required Stream<List<int>> stream,
    required int contentLength,
  })  : field = encodeName(field),
        filename = filename == null ? null : encodeName(filename),
        content = XHttpContent.fromStream(stream, contentLength: contentLength);

  @override
  bool operator ==(Object other) =>
      other is MultipartFile &&
      other.field == field &&
      other.filename == filename &&
      other.contentType.mimeType == contentType.mimeType &&
      other.content == content;

  @override
  int get hashCode => Object.hash(field, filename, contentType.mimeType, content);

  static String encodeName(String value) => value.replaceAll(_newlineRegExp, '%0D%0A').replaceAll('"', '%22');
}
