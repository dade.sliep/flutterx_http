part of '../adapter/adapter.dart';

@immutable
class XHttpRequestOptions {
  final Set group;
  final SecurityContext? context;
  final bool withCredentials;
  final int maxRedirects;
  final Duration? connectionTimeOut;
  final CancellationToken? cancel;
  final RequestProgress? sendProgress;
  final RequestProgress? receiveProgress;

  const XHttpRequestOptions({
    this.group = const {},
    this.context,
    this.withCredentials = false,
    this.maxRedirects = 5,
    this.connectionTimeOut,
    this.cancel,
    this.sendProgress,
    this.receiveProgress,
  }) : assert(maxRedirects >= 0, 'maxRedirects cannot be less than 0');

  static const Object _none = Object();
  static const Duration _noneDuration = Duration(microseconds: -1);

  static void _noneRequestProgress(int loaded, int? total) {}

  XHttpRequestOptions copy({
    Set? group,
    dynamic context = _none,
    bool? withCredentials,
    int? maxRedirects,
    Duration? connectionTimeOut = _noneDuration,
    dynamic cancel = _none,
    RequestProgress? sendProgress = _noneRequestProgress,
    RequestProgress? receiveProgress = _noneRequestProgress,
  }) =>
      XHttpRequestOptions(
        group: group ?? this.group,
        context: identical(context, _none) ? this.context : context,
        withCredentials: withCredentials ?? this.withCredentials,
        maxRedirects: maxRedirects ?? this.maxRedirects,
        connectionTimeOut: identical(connectionTimeOut, _noneDuration) ? this.connectionTimeOut : connectionTimeOut,
        cancel: identical(cancel, _none) ? this.cancel : cancel,
        sendProgress: identical(sendProgress, _noneRequestProgress) ? this.sendProgress : sendProgress,
        receiveProgress: identical(receiveProgress, _noneRequestProgress) ? this.receiveProgress : receiveProgress,
      );
}

typedef RequestProgress = void Function(int loaded, int? total);
