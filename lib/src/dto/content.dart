part of '../adapter/adapter.dart';

@immutable
class XHttpContent {
  final int? contentLength;
  final Stream<List<int>>? _stream;
  final Stream<EventStream>? _eventStream;
  final Uint8List? _bytes;

  const XHttpContent.empty()
      : contentLength = 0,
        _stream = null,
        _eventStream = null,
        _bytes = null;

  const XHttpContent.fromStream(Stream<List<int>> stream, {this.contentLength})
      : assert(contentLength == null || contentLength > 0, 'contentLength cannot be negative'),
        _stream = stream,
        _eventStream = null,
        _bytes = null;

  const XHttpContent.fromEventStream(Stream<EventStream> stream, {this.contentLength})
      : assert(contentLength == null || contentLength > 0, 'contentLength cannot be negative'),
        _stream = null,
        _eventStream = stream,
        _bytes = null;

  const XHttpContent.fromBytes(Uint8List bytes)
      : contentLength = bytes.length,
        _stream = null,
        _eventStream = null,
        _bytes = bytes;

  Future<Uint8List> asBytes() {
    if (_bytes != null) return SynchronousFuture(_bytes!);
    final completer = Completer<Uint8List>();
    final sink = ByteConversionSink.withCallback((bytes) => completer.complete(Uint8List.fromList(bytes)));
    _stream!.listen(sink.add, onError: completer.completeError, onDone: sink.close, cancelOnError: true);
    return completer.future;
  }

  Stream<List<int>> asStream() => _stream ?? StreamView<List<int>>(Stream.value(_bytes!));

  Stream<EventStream> asEventStream() {
    assert(_eventStream != null, 'Add Accept: text/event-stream header in order to use EventStream');
    return _eventStream!;
  }
}
