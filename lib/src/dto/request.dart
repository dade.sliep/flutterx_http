part of '../adapter/adapter.dart';

@immutable
class XHttpRequest {
  final String method;
  final Uri url;
  final HeadersMap headers;
  final XHttpContent? body;

  MediaType get contentType =>
      headers[HttpHeaders.contentTypeHeader]?.let(MediaType.parse) ?? MediaType('application', 'octet-stream');

  Encoding get encoding => contentType.parameters['charset']?.let(Encoding.getByName) ?? utf8;

  const XHttpRequest({
    required this.method,
    required this.url,
    required this.headers,
    required this.body,
  });

  static const XHttpContent _noneContent = XHttpContent.empty();

  XHttpRequest copy({
    String? method,
    Uri? url,
    XHttpContent? body = _noneContent,
  }) =>
      XHttpRequest(
        method: method ?? this.method,
        url: url ?? this.url,
        headers: headers,
        body: identical(body, _noneContent) ? this.body : body,
      );

  @override
  bool operator ==(Object other) =>
      other is XHttpRequest &&
      other.method == method &&
      other.url == url &&
      mapEquals(other.headers, headers) &&
      other.body == body;

  @override
  int get hashCode => Object.hash(method, url, Object.hashAll(headers.keys), Object.hashAll(headers.values), body);

  @override
  String toString() => 'Request($method $url)';
}
