part of '../adapter/adapter.dart';

@immutable
class XHttpResponse {
  final XHttpRequest request;
  final XHttpRequestOptions options;
  final XHttpContent body;
  final int statusCode;
  final String statusMessage;
  final HeadersMap headers;
  final bool isRedirect;

  bool get isSuccess => statusCode ~/ 100 == 2;

  MediaType get contentType =>
      headers[HttpHeaders.contentTypeHeader]?.let(MediaType.parse) ?? MediaType('application', 'octet-stream');

  Encoding get encoding => contentType.parameters['charset']?.let(Encoding.getByName) ?? utf8;

  const XHttpResponse({
    required this.request,
    required this.options,
    required this.body,
    required this.statusCode,
    required this.statusMessage,
    required this.headers,
    required this.isRedirect,
  });

  factory XHttpResponse.create({
    required XHttpRequest request,
    required XHttpRequestOptions options,
    required XHttpContent body,
    int statusCode = 200,
    String statusMessage = 'OK',
    HeadersMap headers = const {},
    bool isRedirect = false,
  }) =>
      XHttpResponse(
          request: request,
          options: options,
          body: body,
          statusCode: statusCode,
          statusMessage: statusMessage,
          headers: headers,
          isRedirect: isRedirect);

  XHttpResponse copy({
    XHttpRequest? request,
    XHttpRequestOptions? options,
    XHttpContent? body,
    int? statusCode,
    String? statusMessage,
    bool? isRedirect,
  }) =>
      XHttpResponse(
        request: request ?? this.request,
        options: options ?? this.options,
        body: body ?? this.body,
        statusCode: statusCode ?? this.statusCode,
        statusMessage: statusMessage ?? this.statusMessage,
        headers: headers,
        isRedirect: isRedirect ?? this.isRedirect,
      );

  @override
  bool operator ==(Object other) =>
      other is XHttpResponse &&
      other.request == request &&
      other.options == options &&
      other.body == body &&
      other.statusCode == statusCode &&
      other.statusMessage == statusMessage &&
      mapEquals(other.headers, headers) &&
      other.isRedirect == isRedirect;

  @override
  int get hashCode => Object.hash(request, options, body, statusCode, statusMessage, Object.hashAll(headers.keys),
      Object.hashAll(headers.values), isRedirect);

  @override
  String toString() => 'Response(${request.method} ${request.url}): $statusCode $statusMessage';
}
