part of '../adapter/adapter.dart';

@immutable
class XHttpException implements Exception {
  final String message;
  final XHttpResponse? response;

  const XHttpException({required this.message, this.response});

  @override
  String toString() => 'HttpError($message, response: $response)';
}
