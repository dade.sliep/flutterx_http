import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutterx_http/flutterx_http.dart';
import 'package:flutterx_http/src/adapter/adapter_io.dart'
    if (dart.library.html) 'package:flutterx_http/src/adapter/adapter_browser.dart';
import 'package:flutterx_utils/flutterx_utils.dart';
import 'package:http_parser/http_parser.dart';

part '../dto/content.dart';
part '../dto/exception.dart';
part '../dto/multipart_file.dart';
part '../dto/options.dart';
part '../dto/event_stream.dart';
part '../dto/request.dart';
part '../dto/response.dart';

abstract class XHttpClient implements XHttpInterceptor {
  final List<XHttpInterceptor> _chain = [];

  factory XHttpClient({String? baseUrl}) {
    final client = XHttpClientAdapter();
    if (baseUrl != null) client.addInterceptor(BaseUrlInterceptor(baseUrl));
    return client;
  }

  XHttpClient.impl();

  void addInterceptor(XHttpInterceptor interceptor, {int? index}) =>
      index == null ? _chain.add(interceptor) : _chain.insert(index, interceptor);

  Future<XHttpResponse> send(XHttpRequest request, {XHttpRequestOptions options = const XHttpRequestOptions()}) =>
      InterceptorChain._([..._chain, this]).call(request, options);

  Future<XHttpResponse> sendImpl(XHttpRequest request, CallImplementation impl,
          {XHttpRequestOptions options = const XHttpRequestOptions()}) =>
      InterceptorChain._([..._chain, XHttpInterceptorImpl(impl)]).call(request, options);

  void close();
}

class InterceptorChain {
  final Iterator<XHttpInterceptor> _iterator;

  InterceptorChain._(Iterable<XHttpInterceptor> chain) : _iterator = chain.toList().iterator;

  Future<XHttpResponse> call(XHttpRequest request, XHttpRequestOptions options) =>
      (_iterator..moveNext()).current.intercept(request, options, this);
}

abstract class XHttpInterceptor {
  const XHttpInterceptor._();

  Future<XHttpResponse> intercept(XHttpRequest request, XHttpRequestOptions options, InterceptorChain next);
}

typedef CallImplementation = Future<XHttpResponse> Function(XHttpRequest request, XHttpRequestOptions options);

class XHttpInterceptorImpl implements XHttpInterceptor {
  final CallImplementation impl;

  const XHttpInterceptorImpl(this.impl);

  @override
  Future<XHttpResponse> intercept(XHttpRequest request, XHttpRequestOptions options, InterceptorChain next) =>
      impl(request, options);
}

extension CompleterExt on Completer {
  void completeResponse(XHttpResponse response) => response.isSuccess
      ? complete(response)
      : completeError(XHttpException(message: 'HTTP Response error', response: response), StackTrace.current);
}
