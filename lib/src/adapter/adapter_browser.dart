// ignore_for_file: avoid_web_libraries_in_flutter

import 'dart:async';
import 'dart:convert';
import 'dart:html' hide VoidCallback;
import 'dart:io' show HttpHeaders;
import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:flutterx_http/flutterx_http.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

class XHttpClientAdapter extends XHttpClient {
  final Set<HttpRequest> _xhrs = {};

  XHttpClientAdapter() : super.impl();

  @override
  Future<XHttpResponse> intercept(XHttpRequest request, XHttpRequestOptions options, InterceptorChain _) {
    final completer = Completer<XHttpResponse>();
    final xhr = HttpRequest();
    options
      ..sendProgress?.let((onProgress) => xhr.upload.onProgress.listenProgress(completer, onProgress))
      ..receiveProgress?.let((onProgress) => xhr.onProgress.listenProgress(completer, onProgress))
      ..cancel?.onCancel((_, __) => xhr.abort());
    xhr
      ..onAbort.listenFirst(completer,
          () => completer.completeError(const XHttpException(message: 'HTTP Request aborted'), StackTrace.current))
      ..onTimeout.listenFirst(completer,
          () => completer.completeError(const XHttpException(message: 'HTTP Request timed out'), StackTrace.current))
      ..onError.listenFirst(completer,
          () => completer.completeError(const XHttpException(message: 'HTTP Request error'), StackTrace.current));
    final eventStream = request.headers[HttpHeaders.acceptHeader] == 'text/event-stream';
    if (eventStream) {
      var processIndex = 0;
      final controller = StreamController<EventStream>();
      completer.future.catchError((e, stackTrace) {
        controller
          ..addError(e, stackTrace)
          ..close();
        throw e;
      });
      xhr.onReadyStateChange.listen((e) => switch (xhr.readyState) {
            HttpRequest.HEADERS_RECEIVED => completer.completeResponse(XHttpResponse(
                request: request,
                options: options,
                body: XHttpContent.fromEventStream(controller.stream),
                statusCode: xhr.status!,
                statusMessage: xhr.statusText ?? '',
                headers: headersMap()..addAll(xhr.responseHeaders),
                isRedirect: const {302, 301}.contains(xhr.status))),
            HttpRequest.DONE => controller.close(),
            _ => null,
          });
      xhr.onProgress.listen((event) {
        final responseText = xhr.responseText ?? '';
        if (processIndex < responseText.length) {
          final newData = responseText.substring(processIndex);
          processIndex = responseText.length;
          EventStream.parseEventStream(LineSplitter.split(newData), controller.sink);
        }
      });
    } else {
      xhr.onLoad.listenFirst(
          completer,
          () => completer.completeResponse(XHttpResponse(
              request: request,
              options: options,
              body: XHttpContent.fromBytes((xhr.response as ByteBuffer).asUint8List()),
              statusCode: xhr.status!,
              statusMessage: xhr.statusText ?? '',
              headers: headersMap()..addAll(xhr.responseHeaders),
              isRedirect: const {302, 301}.contains(xhr.status))));
    }
    xhr
      ..let(_xhrs.add)
      ..open(request.method, request.url.toString(), async: true)
      ..responseType = eventStream ? 'text' : 'arraybuffer'
      ..withCredentials = options.withCredentials
      ..timeout = options.connectionTimeOut?.inMilliseconds;
    request.headers.forEach(xhr.setRequestHeader);
    final body = request.body;
    if (body != null) {
      body.asBytes().then(xhr.send);
    } else {
      xhr.send();
    }
    return completer.future.whenComplete(() => _xhrs.remove(xhr));
  }

  @override
  void close() {
    for (final xhr in _xhrs.toSet()) _xhrs.remove(xhr..abort());
  }
}

extension _StreamExt on Stream<ProgressEvent> {
  void listenFirst(Completer completer, VoidCallback onEvent) {
    late StreamSubscription<ProgressEvent> subscription;
    subscription = listen((event) => subscription.cancel().then((_) => onEvent()),
        onError: completer.completeError, cancelOnError: true);
  }

  void listenProgress(Completer completer, RequestProgress onProgress) =>
      listen((event) => onProgress(event.loaded ?? 0, event.lengthComputable ? null : event.total),
          onError: completer.completeError, cancelOnError: true);
}
