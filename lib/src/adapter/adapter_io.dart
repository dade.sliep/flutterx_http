import 'dart:async';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutterx_http/flutterx_http.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

class XHttpClientAdapter extends XHttpClient {
  final Set<CancellationToken> _requests = {};

  XHttpClientAdapter() : super.impl();

  @override
  Future<XHttpResponse> intercept(XHttpRequest request, XHttpRequestOptions options, InterceptorChain _) {
    final completer = Completer<XHttpResponse>();
    final client = HttpClient(context: options.context)..connectionTimeout = options.connectionTimeOut;
    options.cancel?.onCancel((_, __) => client.close(force: true));
    client.openUrl(request.method, request.url).then((ioRequest) async {
      final body = request.body;
      ioRequest
        ..followRedirects = options.maxRedirects > 0
        ..maxRedirects = options.maxRedirects
        ..contentLength = body?.contentLength ?? -1;
      request.headers.forEach(ioRequest.headers.set);
      if (body != null) {
        final stream = body.asStream();
        final onProgress = options.sendProgress;
        await ioRequest.addStream(onProgress != null
            ? stream.handleProgress(onProgress: (loaded) => onProgress(loaded, body.contentLength))
            : stream);
      }
      final ioResponse = await ioRequest.close();
      final headers = headersMap();
      ioResponse.headers.forEach((key, values) => headers[key] = values.join(','));
      final onProgress = options.receiveProgress;
      final contentLength = ioResponse.contentLength == -1 ? null : ioResponse.contentLength;
      completer.completeResponse(XHttpResponse(
          request: request,
          options: options,
          body: XHttpContent.fromStream(
              onProgress != null
                  ? ioResponse.handleProgress(onProgress: (loaded) => onProgress(loaded, contentLength))
                  : ioResponse,
              contentLength: contentLength),
          statusCode: ioResponse.statusCode,
          statusMessage: ioResponse.reasonPhrase,
          headers: headers,
          isRedirect: ioResponse.isRedirect));
    }, onError: (error, stackTrace) => completer.completeError(XHttpException(message: error.toString()), stackTrace));
    return completer.future;
  }

  @override
  void close() {
    for (final request in _requests.toSet()) _requests.remove(request..cancel());
  }
}

extension _StreamExt on Stream<List<int>> {
  Stream<List<int>> handleProgress({required ValueChanged<int> onProgress}) {
    var loaded = 0;
    return transform(StreamTransformer.fromHandlers(handleData: (data, sink) {
      sink.add(data);
      onProgress(loaded += data.length);
    }));
  }
}
