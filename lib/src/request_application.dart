import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutterx_http/flutterx_http.dart';

@immutable
class Request extends IRequest {
  final String method;
  final Uri url;
  final QueryParameters params;
  final HeadersMap headers;
  final Object? body;
  final Encoding? encoding;

  const Request({
    required this.method,
    required this.url,
    this.params = const {},
    this.headers = const {},
    this.body,
    Encoding this.encoding = utf8,
  });

  const Request.head({
    required this.url,
    this.params = const {},
    this.headers = const {},
  })  : method = 'HEAD',
        body = null,
        encoding = null;

  const Request.get({
    required this.url,
    this.params = const {},
    this.headers = const {},
  })  : method = 'GET',
        body = null,
        encoding = null;

  const Request.post({
    required this.url,
    this.params = const {},
    this.headers = const {},
    this.body,
    Encoding this.encoding = utf8,
  }) : method = 'POST';

  const Request.put({
    required this.url,
    this.params = const {},
    this.headers = const {},
    this.body,
    Encoding this.encoding = utf8,
  }) : method = 'PUT';

  const Request.patch({
    required this.url,
    this.params = const {},
    this.headers = const {},
    this.body,
    Encoding this.encoding = utf8,
  }) : method = 'PATCH';

  const Request.delete({
    required this.url,
    this.params = const {},
    this.headers = const {},
    this.body,
    Encoding this.encoding = utf8,
  }) : method = 'DELETE';

  @override
  XHttpRequest finalize() {
    final headers = headersMap()
      ..[HttpHeaders.contentTypeHeader] = 'application/json'
      ..addAll(this.headers);
    return XHttpRequest(
        method: method, url: url.appendQueryParameters(params), headers: headers, body: _encodedBody(headers));
  }

  XHttpContent? _encodedBody(HeadersMap headers) {
    dynamic body = this.body;
    if (body == null || body is XHttpContent) return body;
    final encoding = this.encoding!;
    if (body is! String) {
      final contentType = headers[HttpHeaders.contentTypeHeader];
      switch (contentType) {
        case 'application/json':
          body = jsonEncode(body);
          break;
        case 'application/x-www-form-urlencoded':
          body = (body as Map)
              .entries
              .map((entry) =>
                  '${Uri.encodeQueryComponent(entry.key, encoding: encoding)}=${Uri.encodeQueryComponent(entry.value, encoding: encoding)}')
              .join('&');
          break;
        default:
          throw UnsupportedError(
              'Failed to encode request body of type ${body.runtimeType} for contentType: $contentType');
      }
    }
    return XHttpContent.fromBytes(Uint8List.fromList(encoding.encode(body)));
  }
}
