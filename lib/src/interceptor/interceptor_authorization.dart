import 'dart:async';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutterx_http/flutterx_http.dart';
import 'package:flutterx_http/src/adapter/adapter.dart';

class AuthorizationInterceptor implements XHttpInterceptor {
  static const String noAuth = 'no_auth';
  final ValueGetter<FutureOr<String?>> authorization;

  const AuthorizationInterceptor(this.authorization);

  @override
  Future<XHttpResponse> intercept(XHttpRequest request, XHttpRequestOptions options, InterceptorChain next) async {
    if (!options.group.contains(noAuth)) {
      final auth = await authorization();
      if (auth != null) request.headers[HttpHeaders.authorizationHeader] = auth;
    }
    return next(request, options);
  }
}
