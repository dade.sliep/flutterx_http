import 'dart:async';

import 'package:flutterx_http/src/adapter/adapter.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

class LoggableInterceptor with Loggable implements XHttpInterceptor {
  static const Set<String> commonMimeTypes = {
    'text/css',
    'text/csv',
    'text/html',
    'text/plain',
    'text/calendar',
    'text/javascript',
    'text/xml',
    'application/json',
    'application/xml',
    'application/x-www-form-urlencoded'
  };
  final Set<String> logMimeTypes;

  LoggableInterceptor({this.logMimeTypes = commonMimeTypes});

  @override
  Future<XHttpResponse> intercept(XHttpRequest request, XHttpRequestOptions options, InterceptorChain next) async {
    try {
      request = _logRequest(request);
      final response = await next(request, options);
      return _logResponse(response);
    } on XHttpException catch (e) {
      final response = e.response;
      if (response != null) throw XHttpException(message: e.message, response: _logResponse(response));
      rethrow;
    }
  }

  XHttpRequest _logRequest(XHttpRequest request) {
    logInfo('HTTP Request ${request.method} ${request.url}', () {
      _logKV('method', request.method);
      _logKV('url', '${request.url.scheme}://${request.url.host}:${request.url.port}${request.url.path}');
      final parameters = request.url.queryParameters;
      if (parameters.isNotEmpty) logDebug('queryParameters:', () => parameters.forEach(_logKV));
      if (request.headers.isNotEmpty) logDebug('headers:', () => request.headers.forEach(_logKV));
      final body = request.body;
      if (body != null) {
        if (logMimeTypes.contains(request.contentType.mimeType)) {
          request = request.copy(body: body.intercept((bytes) {
            try {
              indent.increase();
              _logKV('contentLength', bytes.length);
              logDebug('body', () => logDebug(request.encoding.decode(bytes)));
            } catch (e, stack) {
              logError('Failed to log body', e, stack);
            } finally {
              indent.decrease();
            }
          }));
        } else {
          body.contentLength?.let((value) => _logKV('contentLength', value));
        }
      }
    });
    return request;
  }

  XHttpResponse _logResponse(XHttpResponse response) {
    logInfo('HTTP Response ${response.request.method} ${response.request.url}', () {
      if (response.headers.isNotEmpty) logDebug('headers:', () => response.headers.forEach(_logKV));
      _logKV('statusCode', response.statusCode, level: LogLevel.info);
      if (response.statusMessage.isNotEmpty) _logKV('statusMessage', response.statusMessage, level: LogLevel.info);
      if (logMimeTypes.contains(response.contentType.mimeType)) {
        response = response.copy(body: response.body.intercept((bytes) {
          try {
            indent.increase();
            _logKV('contentLength', bytes.length);
            logDebug('body', () => logDebug(response.encoding.decode(bytes)));
          } catch (e, stack) {
            logError('Failed to log body', e, stack);
          } finally {
            indent.decrease();
          }
        }));
      } else {
        response.body.contentLength?.let((value) => _logKV('contentLength', value));
      }
    });
    return response;
  }

  void _logKV(dynamic key, dynamic value, {LogLevel level = LogLevel.debug}) => log(level, '$key: $value');
}

extension _XHttpContentExt on XHttpContent {
  XHttpContent intercept(void Function(List<int> data) intercept) {
    final result = <int>[];
    return XHttpContent.fromStream(asStream().transform(StreamTransformer.fromHandlers(handleData: (data, sink) {
      result.addAll(data);
      sink.add(data);
    }, handleDone: (sink) {
      intercept(result);
      sink.close();
    })));
  }
}
