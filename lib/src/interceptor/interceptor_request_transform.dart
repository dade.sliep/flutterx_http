import 'package:flutterx_http/flutterx_http.dart';
import 'package:flutterx_http/src/adapter/adapter.dart';

class RequestTransformInterceptor implements XHttpInterceptor {
  final XHttpRequest Function(XHttpRequest request) transform;

  const RequestTransformInterceptor(this.transform);

  @override
  Future<XHttpResponse> intercept(XHttpRequest request, XHttpRequestOptions options, InterceptorChain next) =>
      next(transform(request), options);
}
