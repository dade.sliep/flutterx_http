import 'dart:async';

import 'package:flutterx_http/flutterx_http.dart';
import 'package:flutterx_http/src/adapter/adapter.dart';

class MockInterceptor implements XHttpInterceptor {
  final MockCallHandler handler;

  const MockInterceptor(this.handler);

  @override
  Future<XHttpResponse> intercept(XHttpRequest request, XHttpRequestOptions options, InterceptorChain next) async =>
      await handler(request, options);
}

typedef MockCallHandler = FutureOr<XHttpResponse> Function(XHttpRequest request, XHttpRequestOptions options);
