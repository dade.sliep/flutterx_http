import 'package:flutterx_http/src/adapter/adapter.dart';

class BaseUrlInterceptor implements XHttpInterceptor {
  final String baseUrl;

  BaseUrlInterceptor(String baseUrl)
      : baseUrl = baseUrl.endsWith('/') ? baseUrl.substring(0, baseUrl.length - 1) : baseUrl;

  @override
  Future<XHttpResponse> intercept(XHttpRequest request, XHttpRequestOptions options, InterceptorChain next) {
    if (!request.url.hasScheme) request = request.copy(url: Uri.parse('$baseUrl${request.url}'));
    return next(request, options);
  }
}
