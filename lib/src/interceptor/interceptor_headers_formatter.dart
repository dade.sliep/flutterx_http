import 'dart:async';

import 'package:flutterx_http/flutterx_http.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

class RequestHeadersFormatterInterceptor implements XHttpInterceptor {
  final Set<String>? whitelist;
  final Set<String>? blacklist;

  const RequestHeadersFormatterInterceptor({this.whitelist, this.blacklist})
      : assert(whitelist == null || blacklist == null, 'Cannot set both whitelist and blacklist');

  @override
  Future<XHttpResponse> intercept(XHttpRequest request, XHttpRequestOptions options, InterceptorChain next) {
    _format(request.headers, whitelist, blacklist);
    return next(request, options);
  }
}

class ResponseHeadersFormatterInterceptor implements XHttpInterceptor {
  final Set<String>? whitelist;
  final Set<String>? blacklist;

  const ResponseHeadersFormatterInterceptor({this.whitelist, this.blacklist})
      : assert(whitelist == null || blacklist == null, 'Cannot set both whitelist and blacklist');

  @override
  Future<XHttpResponse> intercept(XHttpRequest request, XHttpRequestOptions options, InterceptorChain next) async {
    try {
      final response = await next(request, options);
      _format(response.headers, whitelist, blacklist);
      return response;
    } on XHttpException catch (e) {
      e.response?.headers.let((headers) => _format(headers, whitelist, blacklist));
      rethrow;
    }
  }
}

final RegExp _word = RegExp('([a-zA-Z]{1,})');
final Map<String, String> _cache = <String, String>{};

void _format(HeadersMap headers, Set<String>? whitelist, Set<String>? blacklist) {
  for (final header in headers.keys.toList()) {
    if (!(blacklist?.contains(header) ?? false) && (whitelist?.contains(header) ?? true)) {
      final value = _cache[header] ??= header.replaceAllMapped(_word, (match) => match[0]!.capitalized);
      if (value != header) headers[value] = headers.remove(header)!;
    }
  }
}
