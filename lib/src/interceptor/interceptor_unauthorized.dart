import 'package:flutterx_http/src/adapter/adapter.dart';

class UnauthorizedInterceptor implements XHttpInterceptor {
  final Set<int> statusCodes;
  final void Function(XHttpException error) onUnauthorized;

  const UnauthorizedInterceptor({this.statusCodes = const {401}, required this.onUnauthorized});

  @override
  Future<XHttpResponse> intercept(XHttpRequest request, XHttpRequestOptions options, InterceptorChain next) async {
    try {
      return await next(request, options);
    } on XHttpException catch (error) {
      if (statusCodes.contains(error.response?.statusCode)) onUnauthorized(error);
      rethrow;
    }
  }
}
